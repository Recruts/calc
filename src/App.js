import React, { useState } from "react";
import { evaluate } from "mathjs";
import "./App.css";

function App() {
    const [input, setInput] = useState("");

    const handleClick = (value) => {
        setInput(input + value);
    };

    const handleClear = () => {
        setInput("");
    };

    const handleCalculate = () => {
        try {
            setInput(evaluate(input).toString());
        } catch (error) {
            setInput("Ошибка");
        }
    };

    const handleNegate = () => {
        setInput((parseFloat(input) * -1).toString());
    };

    const handlePercentage = () => {
        setInput((parseFloat(input) / 100).toString());
    };

    const createButton = (value) => {
        switch (value) {
            case "C":
                return <button onClick={handleClear} key="C">C</button>
            case "+/-":
                return <button onClick={handleNegate} key="+/-">+/-</button>
            case "%":
                return <button onClick={handlePercentage} key="%">%</button>
            default:
                return (
                    <button
                        onClick={() => handleClick(value)}
                        key={value}
                        className={value === "0" ? "zero" : ""}
                    >
                        {value}
                    </button>
                );
        }
    };

    const buttons = [
        "C", "+/-", "%", "/",
        "7", "8", "9", "*",
        "4", "5", "6", "-",
        "1", "2", "3",
    ];

    return (
        <div className="App">
            <h1>Калькулятор</h1>
            <input
                type="text"
                value={input}
                readOnly
                className="input-field"
            />
            <div className="buttons-container">
                {buttons.map(createButton)}
                <button onClick={handleClick.bind(null, "+")} key="+">+</button>
                <button onClick={handleClick.bind(null, "0")} key="0" className="zero">0</button>
                <button onClick={handleClick.bind(null, ".")} key=".">.</button>
                <button onClick={handleCalculate} key="=">=</button>
            </div>
        </div>
    );
}

export default App;